#include <iostream>
#include <fstream>
#include <cppzmq/zmq.hpp>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char ** argv) {
    if (argc == 1) {
        return EXIT_FAILURE;
    }

    // Open the file.
    int file_descriptor;
    if ( (file_descriptor = open(argv[1], O_RDONLY)) == -1 ) {
        perror("open");
        return EXIT_FAILURE;
    }

    // Position the internal pointer to front of the file.
    if (lseek(file_descriptor, 0, SEEK_SET) == -1) {
        perror("");
        return EXIT_FAILURE;
    }

    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);

    socket.connect("tcp://localhost:55051");

    zmq::message_t request_message, reply_message;

    char buffer[BUFSIZ + 1]; // Keep room for null terminator.
    ssize_t bytes_read;

    // Read the file in chunks, sending each chunk to the server for storage.
    while ((bytes_read = read(file_descriptor, buffer, BUFSIZ)) > 0) {
        buffer[bytes_read] = '\0';

        request_message.rebuild((void *) buffer, static_cast<size_t>(bytes_read));
        socket.send(request_message);

        socket.recv(&reply_message);
        std::cout << "Received reply from server\n";
    }

    // Check for a read error. What action can we take here?
    // Can we reset the file pointer and try again?
    if ( bytes_read == -1 ) {
        perror("Read file");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}