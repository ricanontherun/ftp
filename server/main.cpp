#include <fstream>
#include <iostream>
#include <cppzmq/zmq.hpp>

int main() {
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REP);

    socket.bind("tcp://*:55051");

    zmq::message_t message;
    zmq::message_t reply;

    std::ofstream file("/home/ricanontherun/test.txt");

    if (file.bad() || !file.is_open()) {
        std::cerr << "Failed to open file\n";
        return EXIT_FAILURE;
    }

    while (true) {
        socket.recv(&message);

        file.write((char *) message.data(), message.size()).flush();

        // Send the reply back to the client.
        memcpy(reply.data(), "OK", 2);

        socket.send(reply);
    }

    return EXIT_SUCCESS;
}